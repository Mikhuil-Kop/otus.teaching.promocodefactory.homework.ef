﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        public readonly IRepository<Customer> CustomersRepo;
        public readonly IRepository<Preference> PreferencesRepo;
        public readonly IRepository<PromoCode> PromocodesRepo;
        public CustomersController(
            IRepository<Customer> customersRepo,
            IRepository<Preference> preferencesRepo,
            IRepository<PromoCode> promocodesRepo
        )
        {
            CustomersRepo = customersRepo;
            PreferencesRepo = preferencesRepo;
            PromocodesRepo = promocodesRepo;
        }

        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            return Ok(await CustomersRepo.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await CustomersRepo.GetByIdAsync(id);

            if (customer == null)
            {
                return BadRequest();
            }

            return new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCodes?.Select(x => new PromoCodeShortResponse
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToLongDateString(),
                    EndDate = x.EndDate.ToLongDateString(),
                    PartnerName = x.PartnerName
                }).ToList()
            };

        }

        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = request.PreferenceIds == null
                    ? null
                    : (await PreferencesRepo.GetAllAsync())
                        .Where(p => request.PreferenceIds.Exists(p_id => p_id == p.Id))
                        .ToList()
            };

            await CustomersRepo.AddAsync(customer);

            return Ok(customer);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await CustomersRepo.GetByIdAsync(id);
            if (customer == null)
                return BadRequest();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = (await PreferencesRepo.GetAllAsync())
                    .Where(p => request.PreferenceIds.Exists(p_id => p_id == p.Id))
                    .ToList();

            await CustomersRepo.UpdateAsyc(customer);

            return Ok(
                new CustomerResponse
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                    PromoCodes = null,
                    Preferences = customer.Preferences
                        .Select(p => new PreferenceResponse
                        {
                            Id = p.Id,
                            Name = p.Name
                        })
                        .ToList()
                }
            );
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await CustomersRepo.GetByIdAsync(id);
            if (customer == null)
                return BadRequest();

            var promocodes = customer.PromoCodes;

            if (promocodes != null)
            {
                foreach (var promocode in promocodes)
                {
                    await PromocodesRepo.DeleteAsync(promocode.Id);
                }
            }

            await CustomersRepo.DeleteAsync(id);

            return Ok();
        }
    }
}