﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<PromoCode> _promoCodeRepo;
        private IRepository<Customer> _customersRepo;
        private IRepository<Preference> _preferencesRepo;
        private IMapper _mapper;


        public PromocodesController(
            IRepository<PromoCode> promoCodeRepo,
            IRepository<Customer> customersRepo,
            IRepository<Preference> preferencesRepo,
            IMapper mapper
            )
        {
            _promoCodeRepo = promoCodeRepo;
            _customersRepo = customersRepo;
            _preferencesRepo = preferencesRepo;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            return Ok(await _promoCodeRepo.GetAllAsync());
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customers = await _customersRepo.GetAllAsync();
            var preferences = await _preferencesRepo.GetAllAsync();
            var targetCustomers = preferences
                .Where(p => p.Name.Equals(request.Preference))
                .SelectMany(p => p.Customers);

            if (targetCustomers == null)
            {
                return BadRequest("No appropriate customers found!");
            }

            var promocode = await _promoCodeRepo.AddAsync(_mapper.Map<GivePromoCodeRequest, PromoCode>(request));
            targetCustomers.ToList().ForEach(c => c.PromoCodes.Add(promocode));

            foreach (var customer in targetCustomers)
            {
                await _customersRepo.UpdateAsyc(customer);
            }

            return Ok();
        }
    }
}