﻿using System;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
	public class PromoCodeMapper : Profile
	{
		public PromoCodeMapper()
		{
			CreateMap<GivePromoCodeRequest, PromoCode>();
		}
	}
}

