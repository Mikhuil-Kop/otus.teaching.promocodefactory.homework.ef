﻿using System;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
	public class CustomerMapper : Profile
    {
		public CustomerMapper()
		{
            CreateMap<CreateOrEditCustomerRequest, Customer>();
        }
	}
}

