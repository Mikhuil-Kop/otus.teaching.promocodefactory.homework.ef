using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataBase;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using System;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        { 
            services.AddControllers();

            services.AddDbContext<DataBaseContext>(
                options =>
                {
                    var migrationsAssemblyName = typeof(DataBaseContext).GetTypeInfo().Assembly.GetName().Name;
                    var connectionString = Configuration.GetConnectionString("SQLiteConnectionString");
                    options.UseSqlite(connectionString, x => x.MigrationsAssembly(migrationsAssemblyName));
                });

            services.AddScoped<IRepository<Customer>, EfRepository<Customer, DataBaseContext>>();
            services.AddScoped<IRepository<Employee>, EfRepository<Employee, DataBaseContext>>();
            services.AddScoped<IRepository<PromoCode>, EfRepository<PromoCode, DataBaseContext>>();
            services.AddScoped<IRepository<Preference>, EfRepository<Preference, DataBaseContext>>();
            services.AddScoped<IRepository<Role>, EfRepository<Role, DataBaseContext>>();

            services.AddAutoMapper(typeof(CustomerMapper), typeof(PromoCodeMapper));

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.ApplicationServices.Migration();
            app.ApplicationServices.AddFakeData();
        }
    }

    public static class Extensions
    {
        public static void Migration(this IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.CreateScope();

            var databaseContext = scope.ServiceProvider.GetRequiredService<DataBaseContext>();

            // TODO: ��������� � �������
            //databaseContext.ChangeTracker
            //    .Entries()
            //    .ToList()
            //    .ForEach(e => e.State = EntityState.Detached);

            databaseContext.Database.EnsureCreated();
            databaseContext.Database.Migrate();
        }

        public static void AddFakeData(this IServiceProvider serviceProvider)
        {
            using var scope = serviceProvider.CreateScope();

            FakeDataFactory.Initialize(scope.ServiceProvider);
        }
    }
}