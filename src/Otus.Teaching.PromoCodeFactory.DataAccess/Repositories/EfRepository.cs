﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class EfRepository<T, TDbContext> : IRepository<T>
		where T : BaseEntity
		where TDbContext : DbContext
	{
		protected TDbContext Context { get; }
		protected DbSet<T> Set { get; }

		public EfRepository(TDbContext context)
		{
			Context = context;
			Set = Context.Set<T>();
		}

		public virtual async Task<T> GetByIdAsync(Guid Id)
		{
			return await Set.FirstOrDefaultAsync(v => v.Id == Id);
		}

		public virtual async Task<IEnumerable<T>> GetAllAsync()
		{
			return await Set.ToListAsync();
		}

		public virtual async Task<T> AddAsync(T entity)
		{
			Set.Add(entity);

			await Context.SaveChangesAsync();

			return entity;
		}

		public virtual async Task<T> UpdateAsyc(T entity)
		{
			Set.Update(entity);

			await Context.SaveChangesAsync();

			return entity;
		}

		public virtual async Task DeleteAsync(Guid Id)
		{
			var entity = await Set.FirstOrDefaultAsync(v => v.Id == Id);

			Set.Remove(entity);

			await Context.SaveChangesAsync();
		}
	}
}

