﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DataBase
{
    public class DataBaseContext : DbContext
	{
		public DbSet<Employee> Employees { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Preference> Preferences { get; set; }
		public DbSet<PromoCode> PromoCodes { get; set; }

        public DataBaseContext(DbContextOptions<DataBaseContext> dbContextOptions) : base(dbContextOptions)
		{
		}

        public DataBaseContext()
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder
				.Entity<Customer>()
				.HasMany(c => c.Preferences)
				.WithMany(p => p.Customers);

			modelBuilder
				.Entity<PromoCode>()
				.HasOne(p => p.Customer)
				.WithMany(c => c.PromoCodes);
		}
    }
}

