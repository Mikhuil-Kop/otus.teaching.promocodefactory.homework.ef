﻿using System;
namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
	public class CustomerPreference : BaseEntity
	{
		public Guid CustomerID { get; set; }
		public Guid PreferenceID { get; set; }

		public Customer Customer { get; set; }
		public Preference Preference { get; set; }
	}
}

